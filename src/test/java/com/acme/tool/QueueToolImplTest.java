package com.acme.tool;

import com.ibm.mq.MQException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class QueueToolImplTest {
    @Test
    public void getQueueInfo() throws MQException {
        QueueTool tool = createClient();
        QueueInfo queueInfo = tool.getQueueInfo("DEV.QUEUE.1");
        Assert.assertNotNull(queueInfo);
        Assert.assertTrue(queueInfo.getMaximumDepth() > 0);
    }

    @Test
    public void getQueueNames() throws MQException, IOException {
        QueueTool tool = createClient();
        List<String> queueNames = tool.getQueueNames("*");
        Assert.assertNotNull(queueNames);
        Assert.assertTrue(queueNames.size()>0);
    }

    @Test
    public void cleanUp() throws MQException {
        QueueTool tool = createClient();
        tool.cleanUp("DEV.QUEUE.1");
        QueueInfo queueInfo = tool.getQueueInfo("DEV.QUEUE.1");
        Assert.assertNotNull(queueInfo);
        Assert.assertEquals(0, queueInfo.getCurrentDepth());
    }

    @Test
    public void cleanUpQueuesAboveUsage() throws MQException, IOException {
        QueueTool tool = createClient();
        tool.cleanUpQueuesAboveUsage(80, "*");
    }

    private QueueTool createClient() throws MQException {
        String host = "172.17.0.1";
        int port = 1414;
        String channel = "DEV.ADMIN.SVRCONN";
        String qm = "QM1";
        String userId = "admin";
        String password = "passw0rd";
        return new QueueToolImpl(host, port, channel, qm, userId, password);
    }
}