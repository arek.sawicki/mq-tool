package com.acme.tool;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QueueInfoTest {

    @Test
    public void getDepthUsage() {
        QueueInfo info = new QueueInfo(null);
        info.setCurrentDepth(100);
        info.setMaximumDepth(100);
        assertEquals(100, info.getPercentageDepthUsage());

        info.setCurrentDepth(10);
        info.setMaximumDepth(100);
        assertEquals(10, info.getPercentageDepthUsage());

        info.setCurrentDepth(50);
        info.setMaximumDepth(100);
        assertEquals(50, info.getPercentageDepthUsage());

        info.setCurrentDepth(20);
        info.setMaximumDepth(100);
        assertEquals(20, info.getPercentageDepthUsage());

        info.setCurrentDepth(100);
        info.setMaximumDepth(200);
        assertEquals(50, info.getPercentageDepthUsage());
    }
}