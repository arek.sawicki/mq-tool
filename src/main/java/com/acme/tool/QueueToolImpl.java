package com.acme.tool;

import com.ibm.mq.*;
import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.CMQCFC;
import com.ibm.mq.pcf.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mainly based on examples gathered from https://www.capitalware.com/mq_code_java.html
 */
public class QueueToolImpl implements QueueTool {
    private static final Logger logger = LoggerFactory.getLogger(QueueToolImpl.class);

    private MQQueueManager queueManager;
    private PCFAgent agent;

    QueueToolImpl(String hostName, int port, String channel, String queueManager, String userId, String password) throws MQException {
        configureQmAndAgent(hostName, port, channel, queueManager, userId, password);
    }

    QueueToolImpl(ConnectionParams connectionParams) throws MQException {
        this(connectionParams.getHost(), connectionParams.getPort(), connectionParams.getChannel(), connectionParams.getQm(), connectionParams.getUserId(), connectionParams.getPassword());
    }

    @Override
    public QueueInfo getQueueInfo(String queueName) throws MQException {
        MQQueue queue = queueManager.accessQueue(queueName,
                CMQC.MQOO_INQUIRE,
                null,
                null,
                null);
        int currentDepth = queue.getCurrentDepth();
        int maximumDepth = queue.getMaximumDepth();
        QueueInfo info = new QueueInfo(queueName);
        info.setCurrentDepth(currentDepth);
        info.setMaximumDepth(maximumDepth);
        logger.debug("{}", info);
        return info;
    }

    @Override
    public List<String> getQueueNames(String query) throws MQException, IOException {
        List<String> list = new ArrayList<>();
        if (query == null) {
            query = "*";
        }

        PCFParameter[] parameters = {
                new MQCFST(CMQC.MQCA_Q_NAME, query),
                new MQCFIN(CMQC.MQIA_Q_TYPE, CMQC.MQQT_LOCAL)
        };

        MQMessage[] responses = agent.send(CMQCFC.MQCMD_INQUIRE_Q_NAMES, parameters);
        MQCFH cfh = new MQCFH(responses[0]);
        if (cfh.reason == 0) {
            MQCFSL cfsl = new MQCFSL(responses[0]);
            for (int i = 0; i < cfsl.strings.length; i++) {
                String name = cfsl.strings[i];
                if (name != null && !"".equals(name.trim())) {
                    logger.debug("Found {}", name.trim());
                    list.add(name.trim());
                }
            }
        }
        return list;
    }

    @Override
    public void cleanUp(String queueName) throws MQException {
        logger.info("Cleaning up {}", queueName);
        int openOptions = CMQC.MQOO_INQUIRE + CMQC.MQOO_FAIL_IF_QUIESCING + CMQC.MQOO_INPUT_SHARED;
        MQQueue queue = queueManager.accessQueue(queueName, openOptions, null, null, null);
        MQGetMessageOptions getOptions = new MQGetMessageOptions();
        getOptions.options = CMQC.MQGMO_NO_WAIT + CMQC.MQGMO_FAIL_IF_QUIESCING + CMQC.MQGMO_ACCEPT_TRUNCATED_MSG;
        boolean loopAgain = true;
        while (loopAgain) {
            MQMessage message = new MQMessage();
            try {
                queue.get(message, getOptions, 1);
            } catch (MQException e) {
                if (e.completionCode != 1 || e.reasonCode != MQConstants.MQRC_TRUNCATED_MSG_ACCEPTED) {
                    loopAgain = false;
                    if (e.completionCode != 2 || e.reasonCode != MQConstants.MQRC_NO_MSG_AVAILABLE) {
                        logger.error("Failed to cleanUp {}", queueName, e);
                    }
                }
            }
        }
    }

    @Override
    public void cleanUpQueuesAboveUsage(final int threshold, String query) throws MQException, IOException {
        List<String> queues = getStuffyQueues(threshold, query);
        for (String queueName : queues) {
            cleanUp(queueName);
        }
    }

    @Override
    public List<String> getStuffyQueues(final int threshold, String query) throws MQException, IOException {
        List<String> queueNames = getQueueNames(query);
        if (threshold < 1) {
            return queueNames;
        }

        List<QueueInfo> queueInfos = new ArrayList<>();
        for (String q : queueNames) {
            try {
                queueInfos.add(getQueueInfo(q));
            } catch (Exception e) {
                logger.warn("Failed to access {}", q, e);
            }
        }
        return queueInfos.stream()
                .filter(qi -> qi.getPercentageDepthUsage() > threshold)
                .map(QueueInfo::getName)
                .collect(Collectors.toList());
    }

    private void configureQmAndAgent(String hostName, int port, String channel, String queueManager, String userId, String password) throws MQException {
        if (hostName == null) {
            throw new IllegalArgumentException("Null hostname");
        }

        if (port < 1 || port > 65535) {
            throw new IllegalArgumentException("Invalid port " + port);
        }

        if (channel == null) {
            throw new IllegalArgumentException("Null channel");
        }

        if (queueManager == null) {
            throw new IllegalArgumentException("Null queueManager");
        }

        if (userId != null) {
            MQEnvironment.userID = userId;
            MQEnvironment.password = password;
        }

        MQEnvironment.hostname = hostName;
        MQEnvironment.port = port;
        MQEnvironment.channel = channel;
        this.queueManager = new MQQueueManager(queueManager);
        this.agent = new PCFAgent(hostName, port, channel);
    }
}
