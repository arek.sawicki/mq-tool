package com.acme.tool;

import com.ibm.mq.MQException;

import java.io.IOException;
import java.util.List;

public interface QueueTool {
    QueueInfo getQueueInfo(String queueName) throws MQException;
    List<String> getQueueNames(String query) throws MQException, IOException;
    void cleanUp(String queueName) throws MQException;
    void cleanUpQueuesAboveUsage(final int percentage, String query) throws MQException, IOException;
    List<String> getStuffyQueues(final int threshold, String query) throws MQException, IOException;
}
