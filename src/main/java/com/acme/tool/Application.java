package com.acme.tool;

import com.ibm.mq.MQException;

import java.io.IOException;

public interface Application {
    void outputAllQueues() throws MQException, IOException;
    void outputStuffyQueues(int threshold) throws MQException, IOException;
    void clearStuffyQueues(int threshold) throws MQException, IOException;
    void show(String queueName) throws MQException;
}
