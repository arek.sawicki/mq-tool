package com.acme.tool;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class QueueInfo {
    private static  final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    private final String name;
    private int currentDepth;
    private int maximumDepth;

    public QueueInfo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getCurrentDepth() {
        return currentDepth;
    }

    public void setCurrentDepth(int currentDepth) {
        this.currentDepth = currentDepth;
    }

    public int getMaximumDepth() {
        return maximumDepth;
    }

    public void setMaximumDepth(int maximumDepth) {
        this.maximumDepth = maximumDepth;
    }

    public int getPercentageDepthUsage() {
        int max = getMaximumDepth();
        if(max==0) {
            return 100;
        }

        BigDecimal current = BigDecimal.valueOf(getCurrentDepth()).setScale(2, RoundingMode.HALF_UP);
        BigDecimal maximum = BigDecimal.valueOf(getMaximumDepth()).setScale(2, RoundingMode.HALF_UP);

        BigDecimal fraction = current.divide(maximum, RoundingMode.HALF_UP);
        return fraction.multiply(HUNDRED).intValue();
    }

    @Override
    public String toString() {
        return "QueueInfo{" +
                "name='" + name + '\'' +
                ", currentDepth=" + currentDepth +
                ", maximumDepth=" + maximumDepth +
                ", usage=" + getPercentageDepthUsage() +
                '}';
    }
}
