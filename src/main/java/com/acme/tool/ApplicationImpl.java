package com.acme.tool;

import com.ibm.mq.MQException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.List;

@SpringBootApplication
public class ApplicationImpl implements ApplicationRunner, Application {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationImpl.class);
    private static final String ALL_QUEUES = "*";

    private QueueTool queueTool;

    private final ConnectionParams params;

    @Value("${threshold}")
    private int defaultThreshold;

    @Autowired
    public ApplicationImpl(ConnectionParams params) {
        this.params = params;
    }

    public static void main(String[] args) {
        SpringApplication.run(ApplicationImpl.class, args).stop();
    }

    @Override
    public void run(ApplicationArguments args) throws MQException, IOException {
        try {
            queueTool = new QueueToolImpl(params);
        } catch (Exception e) {
            logger.error("Failed to connect", e);
            return;
        }

        int providedThreshold = 0;
        int threshold = defaultThreshold;
        if (args.containsOption("threshold")) {
            String provided = args.getOptionValues("threshold").get(0);
            try {
                providedThreshold = Integer.parseInt(provided);
            } catch (Exception e) {
                logger.warn("Failed to parse provided threshold {}, using default threshold {}", provided, defaultThreshold);
            }
        }

        if (providedThreshold > 0) {
            threshold = providedThreshold;
        }

        if (args.containsOption("cleanup")) {
            clearStuffyQueues(threshold);
        }

        if (args.containsOption("stuffy")) {
            outputStuffyQueues(threshold);
        }

        if (args.containsOption("all")) {
            outputAllQueues();
        }

        if (args.containsOption("show")) {
            List<String> optionValues = args.getOptionValues("show");
            for (String value : optionValues) {
                show(value);
            }
        }
    }

    @Override
    public void outputAllQueues() throws MQException, IOException {
        List<String> queueNames = queueTool.getQueueNames(ALL_QUEUES);
        logger.info("All queues: {}", queueNames);
    }

    @Override
    public void outputStuffyQueues(int threshold) throws MQException, IOException {
        List<String> queueNames = queueTool.getStuffyQueues(threshold, ALL_QUEUES);
        logger.info("Stuffy queues: {}", queueNames);

    }

    @Override
    public void clearStuffyQueues(int threshold) throws MQException, IOException {
        queueTool.cleanUpQueuesAboveUsage(threshold, "*");
    }

    @Override
    public void show(String queueName) throws MQException {
        QueueInfo queueInfo = queueTool.getQueueInfo(queueName);
        logger.info("{}", queueInfo);
    }
}
